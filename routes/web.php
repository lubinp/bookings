<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BookingController@bookings');
Route::post('booking/{booking}', 'BookingController@updateStatus')->name('booking.status');
Route::post('load-more-booking', 'BookingController@loadMoreBooking')->name('booking.paginate');

