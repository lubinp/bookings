<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\NewsletterMail;
use Mail;
use App\User;
class SendNewsLetter implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    
    protected $requests;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    
    public function __construct($requests)
    {
        $this->requests = $requests;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $users = User::whereIn('id', explode(',', $this->requests['userids']))->select('email')->get();
        $email = new NewsletterMail($this->requests);
        foreach ($users as $user) {
            Mail::to($user->email)->send($email);
        }
        
    }
}
