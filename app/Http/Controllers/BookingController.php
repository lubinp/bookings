<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Service;
use App\Booking;
use DB;

class BookingController extends Controller
{
    private $service;

    public function __construct()
    {
        $this->service  = Service::find('1');      // getting the service with id = 1 for all methods  
    }

    public function bookings(Request $request) 
    {
        $service = $this->service;
        $bookings = $service->bookings()->with('user')->get();
        return view('service', compact('service', 'bookings'));
    }

    // route model binding Booking

    public function updateStatus(Request $request, Booking $booking) 
    {
        $booking->update(['status' => $request->status]);
        return redirect()->back()->with(['message' => 'Status Updated Successfully']);
    }

    // load more bookings of all type
    
    public function loadMoreBooking(Request $request)
    {
        $service = $this->service;
        $bookings = $service->bookings()->with('user')
                    ->where('status', strtoupper($request->type))
                    ->paginate(2, ['*'], 'page', $request->page); 
        $html =  view($request->type, compact('bookings'))->render();
        return response()->json(['status' => count($bookings) > 0 ? 1 : 0, 'html' => $html]);
    }
   
}
