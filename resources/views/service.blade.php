@extends('layout')
@section('content')
<div class="container">
   <div class="card" style="margin-top:50px;">
      <div class="card-body">
         <h4 class="card-title">{{ $service->title }}</h4>
         <h6 class="card-subtitle mb-2 text-muted">Price : {{ $service->price }}</h6>
         <div class="media">
            <div class="media-body">
               {!! $service->description !!}
            </div>
            <img class="ml-3" width="20%" src="https://thumbs.dreamstime.com/z/woman-yoga-relax-nature-lake-60434685.jpg" alt="Generic placeholder image">
         </div>
      </div>
      <div class="container">
         <h4 class="card-title">Bookings</h4>
          @if( Session::has("message") )
            <div class="alert alert-success alert-block" role="alert" style="margin-top:30px;">
                <button class="close" data-dismiss="alert"></button>
                {{ Session::get("message") }}
            </div>
          @endif
         <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
               <a class="nav-link active" data-toggle="tab" href="#requests">
                 REQUESTS 
                 <span class="badge badge-danger">{{ $bookings->where('status', 'PENDING')->count() }}</span>
                </a>
            </li>
            <li class="nav-item">
               <a class="nav-link" data-toggle="tab" href="#services">
                 SERVICES
                 <span class="badge badge-danger">{{ $bookings->where('status', 'ACTIVE')->count() }}</span>
                </a>
            </li>
            <li class="nav-item">
               <a class="nav-link" data-toggle="tab" href="#payments">
                 PAYMENTS
                 <span class="badge badge-danger">{{ $bookings->where('status', 'PAYMENT')->count() }}</span>
                </a>
            </li>
         </ul>
         <!-- Tab panes -->
         <div class="tab-content">
            <div id="requests" class="container tab-pane active">
               <br>
               <h3>REQUEST</h3>
                @include('pending')
               <div class="load-data-pending"></div>
               <p class="text-center">
                  <button type="button" class="btn btn-success btn-sm load-more" data-type="pending" data-page="1">Load More</button>
               </p>
            </div>
            <div id="services" class="container tab-pane fade">
               <br>
               <h3>SERVICES</h3>
               @include('active')
               <div class="load-data-active"></div>
               <p class="text-center">
                  <button type="button" class="btn btn-success btn-sm load-more" data-type="active" data-page="1">Load More</button>
               </p>
            </div>
            <div id="payments" class="container tab-pane fade">
               <br>
               <h3>PAYMENTS</h3>
               @include('payments')
               <div class="load-data-payment"></div>
               <p class="text-center">
                  <button type="button" class="btn btn-success btn-sm load-more" data-type="payment" data-page="1">Load More</button>
               </p>
            </div>
         </div>
      </div>
   </div>
</div>
@include('scripts')
@endsection