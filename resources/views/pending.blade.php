@foreach ($bookings->where('status', 'PENDING')->take('2') as $booking)
    <div class="media">
        <div class="media-left">
        <img src="https://www.w3schools.com/bootstrap/img_avatar1.png" class="media-object" style="width:100px">
        </div>
        <div class="media-body" style="padding:10px;">
        <h4 class="media-heading">{{ $booking->user->name }}</h4>
        {{ $booking->booking_date }}-  {{ $booking->id }}
        <form method="post" action="{{ route('booking.status', ['booking'=> $booking->id]) }}">
            @csrf
            <input type="hidden" name="status" value="ACTIVE">
            <button type="submit" class="btn btn-primary btn-sm">ACCEPT REQUEST</button>
        </form>
        </div>
    </div>
@endforeach


