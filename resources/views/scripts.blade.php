<script type="text/javascript">
$(document).ready(function(){
    $('body').on('click', '.load-more', function() {
        page = $(this).attr('data-page');
        page++
        $(this).attr("data-page", page);
        var ths = $(this);
        var type =  $(this).attr('data-type');
        var postData = {
            'type' : type,
            '_token' : '{{ csrf_token() }}',
            'page' : page
        }
        $.ajax({
            url: '{{ route('booking.paginate') }}',
            data: postData,
            type: 'post',
            dataType: 'json',
            success: function(data) {
                if (data.status == 1) {
                    $('.load-data-'+type).append(data.html);
                } else {
                    ths.text('No more data to load')
                }
            },
            error: function(x, h, r) {
                console.log('something went wrong');
            }
        });
    })

    $('.nav-link').click(function(){
        $('.tab-pane').removeClass('active show');
    })

});
</script>