@foreach ($bookings->where('status', 'ACTIVE')->take('2') as $booking)
    <div class="media">
        <div class="media-left">
        <img src="https://www.w3schools.com/bootstrap/img_avatar1.png" class="media-object" style="width:100px">
        </div>
        <div class="media-body" style="padding:20px;">
        <h4 class="media-heading">{{ $booking->user->name }} - {{ $booking->id }}</h4>
        <p>{{ $booking->booking_date }}</p>
        <form method="post" action="{{route('booking.status', ['booking'=> $booking->id])}}">
            @csrf
            <input type="hidden" name="status" value="PAYMENT">
            <button type="submit" class="btn btn-primary btn-sm">GENERATE INVOICE</button>
        </form>
        </div>
    </div>
@endforeach